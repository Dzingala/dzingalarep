<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <title>Visitings amount</title>
      <link rel="stylesheet" href="style/style.css">
  </head>
  <body>
  <jsp:forward page="/jsp/login.jsp"/>
  <div class="wrapper">
      <header>
          <div class="title_header">
              <h1><a href="#nogo">Magazine Styled Blog</a></h1>
              <p>Your Fancy Slogan Goes Here</p>
              (Посещений:
              <%=(request.getAttribute("current_count")==null?"error":request.getAttribute("current_count"))%>
              )
          </div>
          <form action="Add" method="get">
              Enter the 1st number: <input type="text" name ="t1">
              <br>Enter the 2nd number:<input type="text" name = "t2">
              <br>
              <input type="submit" value="Add">
          </form>
          <nav>
              <ul>
                  <li><a class="active" href="#nogo">Home<span class="treug"></span></a></li>
                  <li><a href="#nogo">Business<span class="treug"></span></a></li>
                  <li><a href="#nogo">Entertainment<span class="treug"></span></a></li>
                  <li><a href="#nogo">General<span class="treug"></span></a></li>
                  <li><a href="#nogo">Health<span class="treug"></span></a></li>
                  <li><a href="#nogo">Money<span class="treug"></span></a></li>
                  <li><a href="#nogo">Sports<span class="treug"></span></a></li>
                  <li><a href="#nogo">Technology<span class="treug"></span></a></li>
                  <li><a href="#nogo">Videos<span class="treug"></span></a></li>
                  <li><a href="#nogo">World<span class="treug"></span></a></li>
                  <li><a href="#nogo">Archives<span class="treug"></span></a></li>
              </ul>
          </nav>
      </header>
      <div class="main_content">
          <div class="first_line">
              <div class="frap">
                  <figure>
                      <img src="img/slide.png" alt="Rafael Nadal">
                      <figcaption>
                          <p>
                              <a href="#nogo">Sports</a> -- <time>Wednesday, September 09, 2010, 19:07</time><br>
                              <span>Rafael Nadal, Lorem ipsum dolor amet</span>
                          </p>
                      </figcaption>
                  </figure>
              </div>
              <div class="upcoming_news">
                  <h2>Upcomning News</h2>
                  <div>
                      <img src="img/small.png" alt=" ">
                      <div>
                          <p>Rafael Nadal, Lorem ipsum dolor amet...</p>
                          <time>Wednesday, September 09, 2010, 19:07</time>
                      </div>
                  </div>
                  <div>
                      <img src="img/small.png" alt=" ">
                      <div>
                          <p>Rafael Nadal, Lorem ipsum dolor amet...</p>
                          <time>Wednesday, September 09, 2010, 19:07</time>
                      </div>
                  </div>
                  <div>
                      <img src="img/small.png" alt=" ">
                      <div>
                          <p>Rafael Nadal, Lorem ipsum dolor amet...</p>
                          <time>Wednesday, September 09, 2010, 19:07</time>
                      </div>
                  </div>
                  <div>
                      <img src="img/small.png" alt=" ">
                      <div class="last">
                          <p>Rafael Nadal, Lorem ipsum dolor amet...</p>
                          <time>Wednesday, September 09, 2010, 19:07</time>
                      </div>
                  </div>
              </div>
          </div>
          <div class="second_line">
              <article class="one">
                  <span><a class="red_button" href="#nogo">Sports</a>  -- <time>September 09, 2010, 19:07</time></span>
                  <h2>Rafael Nadal, Lorem ipsum dolor amet</h2>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eu nibh justo, ut posuere nibh. In dictum sapien ac massa aliquam sed tempor massa ultrices.<br><br>
                      Suspendisse consectetur, massa ut sollicitudin ultricies, lectus nisl tempus magna, vitae pellentesque urna sapien sed lacus. Morbi dapibus, erat sit amet laoreet euismod, quam arcu blandit sem, in tempus erat lectus eget risus.
                  </p>
                  <a class="more_info_link" href="#nogo">Read The Full Story</a>
              </article>
              <article class="two">
                  <span><a class="red_button" href="#nogo">World</a>  -- <time>September 10, 2010, 21:00</time></span>
                  <h2>Lorem ipsum dolor sit amet</h2>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eu nibh justo, ut posuere nibh. In dictum sapien ac massa aliquam sed tempor massa ultrices.<br><br>
                      Suspendisse consectetur, massa ut sollicitudin ultricies, lectus nisl tempus magna, vitae pellentesque urna sapien sed lacus. Morbi dapibus, erat sit amet laoreet euismod, quam arcu blandit sem, in tempus erat lectus eget risus.
                  </p>
                  <a class="more_info_link" href="#nogo">Read The Full Story</a>
              </article>
              <article class="three">
                  <span><a class="red_button" href="#nogo">Health</a>  -- <time>September 11, 2010, 22:00</time></span>
                  <h2>Quisque eu nibh justo, ut posuere nibh</h2>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eu nibh justo, ut posuere nibh. In dictum sapien ac massa aliquam sed tempor massa ultrices.<br><br>
                      Suspendisse consectetur, massa ut sollicitudin ultricies, lectus nisl tempus magna, vitae pellentesque urna sapien sed lacus. Morbi dapibus, erat sit amet laoreet euismod, quam arcu blandit sem, in tempus erat lectus eget risus.
                  </p>
                  <a class="more_info_link" href="#nogo">Read The Full Story</a>
              </article>
          </div>
          <div class="third_line">
              <div class="community">
                  <h2>From The Community</h2>
                  <div>
                      <img src="img/message.png" alt=" ">
                      <time>09-09-2010, 19:07</time>
                      <p><span>Steven</span>, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eu nibh justo, ut posuere nibh.</p>
                  </div>
                  <div>
                      <img src="img/message.png" alt=" ">
                      <time>09-09-2010, 19:07</time>
                      <p><span>Stuart</span>, Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
                  <div>
                      <img src="img/message.png" alt=" ">
                      <time>09-09-2010, 19:07</time>
                      <p><span>Vincent</span>, Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
                  <div>
                      <img src="img/message.png" alt=" ">
                      <time>09-09-2010, 19:07</time>
                      <p><span>Sheila</span>, Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  </div>
              </div>

              <div class="latest_photo">
                  <h2>Latest Photo</h2>
                  <div>
                      <img class="fifth_image" src="img/pic1.jpg" alt=" ">
                      <img src="img/pic2.jpg" alt=" " title=" ">
                      <img class="third_image" src="img/pic3.jpg" alt=" ">
                      <img class="fifth_image" src="img/pic4.jpg" alt=" ">
                      <img src="img/pic5.jpg" alt=" " title=" ">
                      <img class="third_image" src="img/pic6.jpg" alt=" ">
                  </div>
              </div>
          </div>
      </div>
      <footer>
          <p>Copyright Magazine Blog, All Rights Reserved</p>
      </footer>
  </div>
  </body>
</html>
