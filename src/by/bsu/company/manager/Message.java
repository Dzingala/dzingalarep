package by.bsu.company.manager;

import java.util.Locale;
import java.util.ResourceBundle;

public enum Message {
    EN(ResourceBundle.getBundle("resources.message", new Locale("en","EN"))),
    RU(ResourceBundle.getBundle("resources.message", new Locale("ru","RU")));
    private ResourceBundle bundle;
    Message(ResourceBundle bundle){this.bundle=bundle;}
    public static String getLocaleMessage(String key,String locale){
        return ResourceBundle.getBundle("resources.message",new Locale(locale.toLowerCase(),locale.toUpperCase())).getString(key);

    }

    public String getMessage(String key){
        return bundle.getString(key);
    }
}
