package by.bsu.company.manager;


import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleManager {
    private static final Locale EN = new Locale("en", "EN");
    private static final Locale RU = new Locale("ru", "RU");

    public static ResourceBundle resourceBundle =
            ResourceBundle.getBundle("resources.message", EN);


    public LocaleManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }

    public static void setResourceBundle(ResourceBundle resourceBundle) {
        LocaleManager.resourceBundle = resourceBundle;
    }

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }
}
