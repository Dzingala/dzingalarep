package by.bsu.company.tag.custom;


import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class CopyrightInfoTag extends TagSupport {
    private static Logger logger = Logger.getLogger(CopyrightInfoTag.class);
    @Override
    public int doStartTag() throws JspException {
        logger.debug("executing doStartTag()");
        String footStart="<footer><hr/>";
        String locale = LocaleManager.getProperty("message.language")+ " "+LocaleManager.getProperty("message.locale")+"<br/><br/>";
        String copyright="Dzingala™ Restaurant® application©";
        String footEnd="<hr/></footer>";
        try {
            JspWriter out = pageContext.getOut();
            out.write(footStart + locale+copyright+footEnd);
        }catch (IOException e){
            logger.error("IOException while creating footer tag");
        }
        return EVAL_BODY_INCLUDE;
    }
    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

}
