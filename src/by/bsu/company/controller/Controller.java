package by.bsu.company.controller;

import by.bsu.company.command.ActionFactory;
import by.bsu.company.command.ActionCommand;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


@WebServlet(urlPatterns = "/controller",name="Controller",
        initParams = {@WebInitParam(name = "log4j", value = "WEB-INF/classes/log4j.properties")})
public class Controller extends HttpServlet {

    private static Logger logger = Logger.getLogger(Controller.class);

    @Override
    public void init()throws ServletException{
        System.setProperty("rootPath", getServletContext().getRealPath("/"));
        String prefix = getServletContext().getRealPath("/");
        String fileName = getInitParameter("log4j");
        if (fileName != null) {
            PropertyConfigurator.configure(prefix + File.separator + fileName);
        }
        logger.debug("Logger initialized.");
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = null;
        // defining the command from JSP
        ActionCommand command = ActionFactory.defineCommand(request);
         /* calling execute() and giving the parameters to the class of certain command */
        page = command.execute(request); // method returns the answer page
        logger.debug(page +" page returned after executing the request");
        if (page != null) {
            String lang = request.getParameter("lang");
            request.setAttribute("lang",lang);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            // calling the answer-page of request
            dispatcher.forward(request, response);
        }
        // setting error page
        else {
            logger.error("null page returned after executing the command");
            page = ConfigurationManager.getProperty("path.page.index");
            request.getSession().setAttribute("nullPage", LocaleManager.getProperty("message.nullpage"));//Message.RU.getMessage("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }
    }

}





