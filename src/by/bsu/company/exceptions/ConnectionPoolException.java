package by.bsu.company.exceptions;

import java.sql.SQLException;

public class ConnectionPoolException extends SQLException {
    public ConnectionPoolException(){}
    public ConnectionPoolException(String message){
        super(message);
    }
    public ConnectionPoolException(InterruptedException e)  {
        super();
    }
    public ConnectionPoolException(SQLException e) {
        super();
    }
}
