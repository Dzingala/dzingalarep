package by.bsu.company.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/ControllerFilter")
public class RequestFilter implements Filter {
    private FilterConfig config = null;
    private static Logger logger = Logger.getLogger(RequestFilter.class);
    private boolean active = false;
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void init (FilterConfig config) throws ServletException
    {
        System.out.println("in filter");
        this.config = config;
        System.out.println("config="+config);
        String act = config.getInitParameter("active");
        System.out.println("act="+act);
        if (act != null){
            System.out.println("active");
            active = (act.toUpperCase().equals("TRUE"));
            System.out.println(active);
        }

        active=true;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void doFilter (ServletRequest request, ServletResponse response,
                          FilterChain chain) throws IOException,
            ServletException
    {
        logger.debug("filtering request with activeStatus="+active);
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res=(HttpServletResponse) response;
        if (active)
        {
            HttpSession session = req.getSession(false);

            if (session == null){
                logger.debug("session is null, redirecting to the login page");
                res.sendRedirect("/jsp/login.jsp");
            }else{
                // pass the request along the filter chain
                chain.doFilter(request, response);
            }
        }


    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   public void destroy()
    {
        config = null;
    }
}
