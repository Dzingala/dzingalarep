package by.bsu.company.command;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class SessionRequestContent {
    private HashMap<String, Object> requestAttributes;
    private HashMap<String, String[]> requestParameters;
    private HashMap<String, Object> sessionAttributes;
    // constructors
    // method of extracting the values from the request
    public void extractValues(HttpServletRequest request) {
        // realisation
    }
    // adding info in request for JSP
    public void insertAttributes(HttpServletRequest request) {
        // realisation
    }
}
