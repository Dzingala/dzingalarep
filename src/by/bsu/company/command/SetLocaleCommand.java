package by.bsu.company.command;

import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;


public class SetLocaleCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(SetLocaleCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing SetLocaleCommand");
        String page=request.getParameter("page");
        String lang=request.getParameter("lang");//getting the language
        logger.debug(lang +" language was chosen");
        LocaleManager.setResourceBundle(ResourceBundle.getBundle("resources.message",new Locale(lang,lang.toUpperCase())));
        System.out.println("bundle created");
        HttpSession session=request.getSession();//getting the session
        session.setAttribute("lang",lang);//setting language into the session
        System.out.println("Page="+page);
        request.setAttribute("localeChangeMessage",LocaleManager.getProperty("message.localeChangeMessage"));
        request.setAttribute("asRegistrated",LocaleManager.getProperty("message.asRegistrated"));
        request.setAttribute("login",LocaleManager.getProperty("message.login"));
        request.setAttribute("password",LocaleManager.getProperty("message.password"));
        return page;

    }
}
