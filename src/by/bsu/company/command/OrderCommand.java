package by.bsu.company.command;


import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.dao.login.LoginDAO;
import by.bsu.company.logic.LoginLogic;
import by.bsu.company.logic.OrderLogic;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class OrderCommand implements ActionCommand {
    private static final String PARAM_NAME_SELECTED="selected";
    private static final String PARAM_NAME_AMOUNT="amount";
    private static final String PARAM_NAME_MENU="menu";

    private static Logger logger = Logger.getLogger(OrderCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing OrderCommand");
        String page = null; // extracting login and password from the request
        String[] checkboxValues = request.getParameterValues(PARAM_NAME_SELECTED);
        String[] amounts =request.getParameterValues(PARAM_NAME_AMOUNT);
        String[] dishes =request.getParameterValues(PARAM_NAME_MENU);
        int userCash = LoginLogic.getUserCash(request.getSession().getAttribute("user").toString());
        for(int i=0;i<dishes.length;i++){
            System.out.println(" dish:" + dishes[i]+", amount:" + amounts[i]+";");
        }
        request.getSession().removeAttribute("order");
        Map<String,String[]> menu = MenuOrderDAO.getUserMenu();
        if(userCash>0) {
            if (OrderLogic.checkOrderValues(checkboxValues, amounts, dishes)) {
                Map<String, int[]> order = new TreeMap<>();
                int totalPrice = OrderLogic.setOrder(order, menu, checkboxValues, amounts);
                if (totalPrice == -1) {
                    request.getSession().removeAttribute("order");
                    request.setAttribute("noAmountsMessage", LocaleManager.getProperty("message.noAmounts"));
                    request.setAttribute("costs", LocaleManager.getProperty("message.costs"));
                    request.setAttribute("list", menu);
                    request.setAttribute("userOrders", LoginDAO.getLoginOrders(request.getSession().getAttribute("user").toString()));
                    request.setAttribute("cash", userCash);
                    page = ConfigurationManager.getProperty("path.page.main");
                    return page;
                }
                request.setAttribute("orderReadyMessage", LocaleManager.getProperty("message.orderReadyMessage"));
                request.setAttribute("costMessage", LocaleManager.getProperty("message.costMessage") + totalPrice + '.');
                request.getSession().setAttribute("order", order);
                request.setAttribute("sendButton", "<form name = \"sendButton\" method=\"POST\" action=\"controller\" >" +
                        "<input type=\"hidden\" name=\"command\" value=\"send\" /><input type=\"submit\" value=\"" +
                        LocaleManager.getProperty("message.sendButton") +
                        "\" > </form>");
                int difference = OrderLogic.matchOrderWithCash(totalPrice, userCash);
                request.getSession().setAttribute("totalPrice",totalPrice);
                if (difference < 0) {
                    request.setAttribute("warningMessage", LocaleManager.getProperty("message.warningMessage"));
                    request.setAttribute("debt", difference);
                }
            } else {
                logger.debug("no checkbox checked");
                request.getSession().removeAttribute("order");
                request.setAttribute("noneSelectedMessage", LocaleManager.getProperty("message.noneSelectedMessage"));

            }
        }else{
            request.setAttribute("ordersUnavailableMessage", LocaleManager.getProperty("message.ordersUnavailable"));
        }
        request.setAttribute("cash",userCash);
        request.setAttribute("costs",LocaleManager.getProperty("message.costs"));
        request.setAttribute("list",menu);
        request.setAttribute("userOrders", LoginDAO.getLoginOrders(request.getSession().getAttribute("user").toString()));
        page= ConfigurationManager.getProperty("path.page.main");
        return page;
    }
}
