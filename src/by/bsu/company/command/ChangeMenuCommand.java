package by.bsu.company.command;

import by.bsu.company.dao.EditMenuDAO;
import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.dao.AdminContentDAO;
import by.bsu.company.logic.ChangeMenuLogic;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.TreeMap;

public class ChangeMenuCommand implements ActionCommand {
    private static final String PARAM_NAME_DISH_TO_ADD = "dishToAdd";
    private static final String PARAM_NAME_DISH_TO_DEL = "dishToDel";
    private static final String PARAM_NAME_PRICE = "price";

    private static Logger logger = Logger.getLogger(ChangeMenuCommand.class);

    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing ChangeMenuCommand");
        String page = null;
        String toAdd= request.getParameter(PARAM_NAME_DISH_TO_ADD);
        String toDel = request.getParameter(PARAM_NAME_DISH_TO_DEL);
        String price = request.getParameter(PARAM_NAME_PRICE);
        Map<String,String[]> menu = (TreeMap<String,String[]>)request.getSession().getAttribute("list");
        request.getSession().removeAttribute("list");
        System.out.println(toAdd+price+toDel);
        if(toAdd!=null  && price!=null && !toAdd.equals("") && !price.equals("")){
            if(!ChangeMenuLogic.checkPrice(price)){
                logger.error("wrong input while editing menu");
                request.setAttribute("editMenuWrongInput",LocaleManager.getProperty("message.editMenuWrongInput"));
            }
            else {
                if (EditMenuDAO.addDish(toAdd, Integer.parseInt(price))) {
                    logger.debug("Dish to add: " + toAdd);
                    request.setAttribute("addDishSuccess", LocaleManager.getProperty("message.addDishSuccess"));

                } else {
                    request.setAttribute("addDishFail", LocaleManager.getProperty("message.addDishFail"));
                }
            }
        }
        if(toAdd==null && toDel!=null && price==null && !toDel.equals("")){
            String dishStatusString = menu.get(toDel)[2];
            System.out.println("status of "+ toDel+":"+dishStatusString);
            boolean dishStatus = !dishStatusString.equals(LocaleManager.getProperty("message.deleteDish"));
            if(EditMenuDAO.editDishStatus(toDel,dishStatus)){
                String[] temp = menu.get(toDel);
                temp[2] = dishStatus?LocaleManager.getProperty("message.deleteDish"):LocaleManager.getProperty("message.enableDish");
                temp[1]= dishStatus? LocaleManager.getProperty("message.available"):LocaleManager.getProperty("message.unavailable");
                menu.put(toDel,temp);
                request.setAttribute("delDishSuccess",LocaleManager.getProperty("message.delDishSuccess"));
            }
            else {
                request.setAttribute("delDishFail",LocaleManager.getProperty("message.delDishFail"));
            }
        }
        HttpSession session = request.getSession();
        session.setAttribute("list",MenuOrderDAO.getAdminMenu());
        session.setAttribute("unconfirmedOrders", AdminContentDAO.getUnconfirmedOrders());
        page = ConfigurationManager.getProperty("path.page.add");
        return page;
    }
}
