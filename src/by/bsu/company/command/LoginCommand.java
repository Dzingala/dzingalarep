package by.bsu.company.command;


import by.bsu.company.dao.AdminContentDAO;
import by.bsu.company.dao.login.LoginDAO;
import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.logic.LoginLogic;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    private static Logger logger = Logger.getLogger(LoginCommand.class);
    @Override
    public String execute(HttpServletRequest request) {
        logger.debug("executing LoginCommand");
        String page = null; // extracting login and password from the request
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        logger.debug("extracted values:\tlogin: "+login+",\tpassword: "+(pass==null?"null":"not null"));
        //checking login and password
        if (LoginLogic.checkLogin(login, pass)) {
            if(LoginLogic.isAdmin()){
                logger.debug("admin page entered");
                HttpSession session = request.getSession();
                session.setAttribute("singleConfirmButton",LocaleManager.getProperty("message.singleConfirmButton"));
                session.setAttribute("unconfirmedOrders", AdminContentDAO.getUnconfirmedOrders());
                session.setAttribute("user", login);
                session.setAttribute("role","administrator");
                session.setAttribute("users",AdminContentDAO.getUsersList());
                session.setAttribute("list", MenuOrderDAO.getAdminMenu());
                session.setAttribute("confirmPage",ConfigurationManager.getProperty("path.page.confirm"));
                session.setAttribute("banPage",ConfigurationManager.getProperty("path.page.ban"));
                session.setAttribute("addPage",ConfigurationManager.getProperty("path.page.add"));
                page = ConfigurationManager.getProperty("path.page.ban");
            }
            else {
                if(LoginDAO.checkLoginAccess(login)){
                    HttpSession session = request.getSession();
                    session.removeAttribute("order");
                    session.setAttribute("role", "user");
                    session.setAttribute("user", login);
                    request.setAttribute("cash",LoginLogic.getUserCash(login));
                    request.setAttribute("userOrders", LoginDAO.getLoginOrders(login));
                    request.setAttribute("list", MenuOrderDAO.getUserMenu());
                    session.setAttribute("costs", LocaleManager.getProperty("message.costs"));
                    page = ConfigurationManager.getProperty("path.page.main");
                }
                else{
                    request.setAttribute("errorLoginPassMessage",LocaleManager.getProperty("message.accountBanned"));
                    page = ConfigurationManager.getProperty("path.page.login");
                }
            }
        } else {
            if(login==null)request.setAttribute("emptyLoginMessage",LocaleManager.getProperty("message.login.empty"));
            request.setAttribute("errorLoginPassMessage", LocaleManager.getProperty("message.loginerror"));
            page = ConfigurationManager.getProperty("path.page.login");
        }
        return page;
    }
}