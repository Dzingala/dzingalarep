package by.bsu.company.command;

import by.bsu.company.dao.AdminContentDAO;
import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.TreeMap;


public class BanCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN_TO_BAN = "loginToBan";
    //private static final String PARAM_NAME_BAN_BUTTON_VALUE="banButton";
    private static Logger logger = Logger.getLogger(BanCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("Executing BanCommand");
        String login=request.getParameter(PARAM_NAME_LOGIN_TO_BAN);
        Map<String,String[]> users = (TreeMap<String,String[]>)request.getSession().getAttribute("users");
        String banButtonValue=users.get(login)[2];
        logger.debug("Ban button value extracted: "+banButtonValue);
        logger.debug("Username extracted: "+login);
        request.getSession().removeAttribute("users");
        if(banButtonValue.equals(LocaleManager.getProperty("message.banButton"))) {
            if(AdminContentDAO.banUser(login,false)) {
                request.setAttribute("banSuccessMessage", LocaleManager.getProperty("message.banSuccess1") + " " + login + " " + LocaleManager.getProperty("message.banSuccess2"));
            } else {
                System.out.println("unlucky");
                request.setAttribute("banSuccessMessage", LocaleManager.getProperty("message.noUserToBan1") + " " + login + " " + LocaleManager.getProperty("message.noUserToBan2"));
            }
        }else if(banButtonValue.equals(LocaleManager.getProperty("message.unbanButton"))) {
            if (AdminContentDAO.banUser(login, true)) {
                request.setAttribute("banSuccessMessage", LocaleManager.getProperty("message.banSuccess1") + " " + login + " " + LocaleManager.getProperty("message.unban2"));
            }else {
                logger.error("error banning user");
            }
        }else{
            request.setAttribute("banSuccessMessage", LocaleManager.getProperty("message.smthWrongWarning"));
        }
        request.getSession().setAttribute("users",AdminContentDAO.getUsersList());

        request.setAttribute("singleConfirmButton",LocaleManager.getProperty("message.singleConfirmButton"));

        return ConfigurationManager.getProperty("path.page.ban");
    }
}
