package by.bsu.company.command;


import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddLinkCommand implements ActionCommand{
    private static Logger logger = Logger.getLogger(AddLinkCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing AddLinkCommand");
        return request.getSession().getAttribute("addPage").toString();
    }
}
