package by.bsu.company.command;

import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.md5util.MD5Hash;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;


public class RegisterLinkCommand implements ActionCommand {
    private static Logger logger = Logger.getLogger(RegisterLinkCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing RegisterLinkCommand");
        return ConfigurationManager.getProperty("path.page.register");
    }
}
