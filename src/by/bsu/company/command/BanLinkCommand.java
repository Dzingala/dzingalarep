package by.bsu.company.command;

import by.bsu.company.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class BanLinkCommand implements ActionCommand{
    private static Logger logger = Logger.getLogger(BanLinkCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing BanLinkCommand");
        return request.getSession().getAttribute("banPage").toString();
    }
}
