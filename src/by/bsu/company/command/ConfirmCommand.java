package by.bsu.company.command;

import by.bsu.company.dao.ConfirmOrdersDAO;
import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.dao.AdminContentDAO;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ConfirmCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN_TO_CONFIRM="loginToConfirm";
    private static final String PARAM_NAME_ORDER_NUM_TO_CONFIRM="orderNumberToConfirm";
    private static Logger logger = Logger.getLogger(ConfirmCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing ConfirmCommand");
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN_TO_CONFIRM);
        int orderNumber=Integer.parseInt(request.getParameter(PARAM_NAME_ORDER_NUM_TO_CONFIRM));
        logger.debug("request to confirm order number "+orderNumber+" of login "+login);
        HttpSession session = request.getSession();
        //String username = session.getAttribute("user").toString();
        if(AdminContentDAO.getUnconfirmedOrders().isEmpty()){
            request.setAttribute("confirmFailMessage",LocaleManager.getProperty("message.confirmFail"));
        }
        else if(ConfirmOrdersDAO.confirmOrder(login, orderNumber)){
            request.setAttribute("unconfirmedOrders", AdminContentDAO.getUnconfirmedOrders());
            request.setAttribute("confirmSuccessMessage",LocaleManager.getProperty("message.confirmSuccess"));

        }
        request.setAttribute("deleteButton",LocaleManager.getProperty("message.deleteDish"));
        request.setAttribute("singleConfirmButton",LocaleManager.getProperty("message.singleConfirmButton"));
        session.removeAttribute("unconfirmedOrders");
        page = ConfigurationManager.getProperty("path.page.confirm");
        return page;
    }
}
