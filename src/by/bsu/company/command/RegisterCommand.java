package by.bsu.company.command;

import by.bsu.company.dao.RegistrationDAO;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class RegisterCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_REPEATED_PASSWORD="repeatpassword";
    private static Logger logger = Logger.getLogger(RegisterCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing RegisterCommand");
        String page = null; // extracting login&pass from request
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        String repeatPass=request.getParameter(PARAM_NAME_REPEATED_PASSWORD);
        if(!pass.equals(repeatPass)){
            logger.error("passwords mismatch while registering");
            request.setAttribute("passwordsMismatch",LocaleManager.getProperty("message.passwordMismatch"));
            page = ConfigurationManager.getProperty("path.page.register");
            return page;
        }
        // checking login&pass
        if (RegistrationDAO.checkLogin(login, pass)) {
            logger.debug("registration success");
            request.setAttribute("user", login);
            request.setAttribute("successRegistrationMessage",LocaleManager.getProperty("message.registrationsuccess"));
        } else {
            logger.error("registration error");
            request.setAttribute("errorRegistrationMessage", LocaleManager.getProperty("message.registrationerror"));
        }

        page = ConfigurationManager.getProperty("path.page.register");
        return page;
    }

}
