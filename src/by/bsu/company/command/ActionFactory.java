package by.bsu.company.command;



import by.bsu.company.manager.LocaleManager;

import javax.servlet.http.HttpServletRequest;


public class ActionFactory {
    public static ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand(); // extracting command name
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) { // if the command is not set
            return current;
        } // getting an object relevant to the command
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            //request.setAttribute("wrongAction", action + Message.RU.getMessage("message.wrongaction"));
            request.setAttribute("wrongAction", action + LocaleManager.getProperty("message.wrongaction"));
        }
        return current;
}

}
