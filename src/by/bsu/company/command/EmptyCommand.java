package by.bsu.company.command;

import by.bsu.company.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 07.10.2015.
 */
public class EmptyCommand implements ActionCommand {
    private static Logger logger = Logger.getLogger(EmptyCommand.class);
    @Override public String execute(HttpServletRequest request) {
    /* in the case of error or appeal directly to the controller */
        logger.debug("executing EmptyCommand");
        String page = ConfigurationManager.getProperty("path.page.login");
        return page;
    }
}
