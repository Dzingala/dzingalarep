package by.bsu.company.command;

import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.dao.SendOrderDAO;
import by.bsu.company.dao.login.LoginDAO;
import by.bsu.company.logic.LoginLogic;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.TreeMap;

public class SendCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(SendCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing SendCommand");
        String page=null; // extracting login&pass from request
        HttpSession session=request.getSession();
        String username = session.getAttribute("user").toString();
        Map<String,String[]> menu = MenuOrderDAO.getUserMenu();
        Map<String,int[]> order=(TreeMap<String,int[]>)request.getSession().getAttribute("order");
        int userCash = LoginLogic.getUserCash(username);
        int totalPrice = (int)request.getSession().getAttribute("totalPrice");
        if(order.isEmpty()){
            logger.debug("empty order");
            request.setAttribute("noneSelectedMessage",LocaleManager.getProperty("message.noneSelectedMessage"));
            request.setAttribute("list",menu);
            request.setAttribute("costs",LocaleManager.getProperty("message.costs"));
            request.setAttribute("sendStatusMessage",LocaleManager.getProperty("message.sendStatusMessage"));
            request.getSession().removeAttribute("order");
            page = ConfigurationManager.getProperty("path.page.main");
            return page;
        }
        request.getSession().removeAttribute("order");
        logger.debug("order consists of:");
        for(Map.Entry< String, int[] > entry : order.entrySet()){
            logger.debug(entry.getKey()+": "+entry.getValue()[0]+" for "+entry.getValue()[1]);
        }
        if(SendOrderDAO.sendOrder(order, username, userCash-totalPrice)){
            logger.debug("send order success.");
        }

        logger.debug("username from session:" + username);
        page = ConfigurationManager.getProperty("path.page.main");
        request.setAttribute("cash", LoginLogic.getUserCash(request.getSession().getAttribute("user").toString()));
        request.setAttribute("list",menu);
        request.setAttribute("costs",LocaleManager.getProperty("message.costs"));
        request.setAttribute("sentMessage",LocaleManager.getProperty("message.sendStatusMessage"));
        request.setAttribute("userOrders", LoginDAO.getLoginOrders(username));

        return page;
    }
}
