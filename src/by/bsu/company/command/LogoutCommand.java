package by.bsu.company.command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class LogoutCommand implements ActionCommand {
    private static final String PATH_PAGE_INDEX="/index.jsp";
    private static Logger logger = Logger.getLogger(LogoutCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing logout command");
        //request.getSession().invalidate();
        //request.getSession(false);*/
        /*request.setAttribute("login", LocaleManager.getProperty("message.login"));
        request.setAttribute("password",LocaleManager.getProperty("message.password"));
        request.setAttribute("asRegistrated",LocaleManager.getProperty("message.asRegistrated"));*/
        invalidateSession(request.getSession(true));
        logger.debug("invalidated "+request.getSession().getId());
        request.getSession().invalidate();

        return PATH_PAGE_INDEX;
    }
    /*This should remove from the session all beans whose name ends in "Bean"
    and all the other attribute in the extraAttr array.*/
    public static void invalidateSession(HttpSession session) {
        String[] extraAttrs = { "loggedUserInfo", };
        java.util.Enumeration attrs = session.getAttributeNames();
        while (attrs.hasMoreElements()) {
            String attr = (String)attrs.nextElement();
            if (attr.endsWith("Bean")) {
                session.removeAttribute(attr);
            }
        }
        for (String extraAttr : extraAttrs) session.removeAttribute(extraAttr);
    }
}
