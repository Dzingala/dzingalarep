package by.bsu.company.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 07.10.2015.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}
