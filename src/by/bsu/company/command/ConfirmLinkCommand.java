package by.bsu.company.command;


import by.bsu.company.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ConfirmLinkCommand implements ActionCommand{
    private static Logger logger = Logger.getLogger(ConfirmLinkCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing ConfirmLinkCommand");
        return request.getSession().getAttribute("confirmPage").toString();
    }
}
