package by.bsu.company.command;

import by.bsu.company.dao.AddResourcesDAO;
import by.bsu.company.dao.MenuOrderDAO;
import by.bsu.company.dao.login.LoginDAO;
import by.bsu.company.logic.AddResourcesLogic;
import by.bsu.company.logic.LoginLogic;
import by.bsu.company.manager.ConfigurationManager;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class AddResourcesCommand implements ActionCommand {
    private static final String PARAM_NAME_CARD_NUMBER="cardNumber";
    private static final String PARAM_NAME_MONEY_AMOUNT="moneyAmount";
    private static Logger logger = Logger.getLogger(AddResourcesCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        String login = request.getSession().getAttribute("user").toString();
        String price = String.valueOf(request.getParameter(PARAM_NAME_MONEY_AMOUNT));
        String cardNumber=String.valueOf(request.getParameter(PARAM_NAME_CARD_NUMBER));
        if(AddResourcesLogic.checkCardNumber(cardNumber)) {
            if (AddResourcesLogic.checkPrice(price)) {
                if (AddResourcesDAO.addResources(login, Integer.parseInt(price),cardNumber)) {
                    request.setAttribute("addingResourcesStatusMessage", LocaleManager.getProperty("message.addResSuccess"));
                }
                else{
                    request.setAttribute("addingResourcesStatusMessage",LocaleManager.getProperty("message.cardLack"));
                }
            } else {
                request.setAttribute("addingResourcesStatusMessage", LocaleManager.getProperty("message.priceTitle"));
            }
        }else{
            request.setAttribute("addingResourcesStatusMessage",LocaleManager.getProperty("message.priceAddTitle"));
        }
        request.setAttribute("cash",LoginLogic.getUserCash(login));
        request.setAttribute("userOrders", LoginDAO.getLoginOrders(login));
        request.setAttribute("list", MenuOrderDAO.getUserMenu());
        request.setAttribute("cash", LoginLogic.getUserCash(login));
        logger.debug("Executing AddResourcesCommand");
        return ConfigurationManager.getProperty("path.page.main");
    }
}
