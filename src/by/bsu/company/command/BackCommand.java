package by.bsu.company.command;

import by.bsu.company.manager.ConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class BackCommand implements ActionCommand {
    private static Logger logger = Logger.getLogger(BackCommand.class);
    @Override
    public String execute(HttpServletRequest request){
        logger.debug("executing BackCommand");
        return ConfigurationManager.getProperty("path.page.login");
    }
}
