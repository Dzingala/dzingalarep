package by.bsu.company.command;


public enum CommandEnum {
    LOGIN{
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT{
        {
            this.command = new LogoutCommand();
        }
    },
    REGISTER{
        {
            this.command=new RegisterCommand();
        }
    },
    LOCALE{
        {
            this.command= new SetLocaleCommand();
        }
    },
    REGISTRATION{
        {
            this.command=new RegisterLinkCommand();
        }
    },
    ORDER{
        {
            this.command = new OrderCommand();
        }
    },
    SEND{
        {
            this.command = new SendCommand();
        }
    },
    CONFIRM{
        {
            this.command=new ConfirmCommand();
        }
    },
    CHANGE{
        {
            this.command = new ChangeMenuCommand();
        }
    },
    BACK{
        {
            this.command= new BackCommand();
        }
    },
    BAN{
        {
            this.command= new BanCommand();
        }
    },
    ADDRESOURCES{
        {
            this.command=new AddResourcesCommand();
        }
    },
    CONFIRMLINK{
        {
            this.command=new ConfirmLinkCommand();
        }
    },
    ADDLINK{
        {
            this.command=new AddLinkCommand();
        }
    },
    BANLINK{
        {
            this.command=new BanLinkCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand(){
        return command;
    }
}
