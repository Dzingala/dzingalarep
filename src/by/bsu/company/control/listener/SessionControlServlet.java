package by.bsu.company.control.listener;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/redirectionservlet",name = "Redirection")
public class SessionControlServlet extends HttpServlet {
    private static Logger logger = Logger.getLogger(SessionControlServlet.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("Executing doGet() in SessionControlServlet");
        request.getSession().setAttribute("lang","en");
        request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
    }
}
