package by.bsu.company.control.listener;

import by.bsu.company.connection.ConnectionPool;
import by.bsu.company.exceptions.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class SessionContextListener implements ServletContextListener {
    private static Logger logger = Logger.getLogger(SessionContextListener.class);
    @Override
    public void contextInitialized(ServletContextEvent httpSessionEvent) {
        logger.debug("Session Created: ");
        ConnectionPool.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent httpSessionEvent) {
        logger.debug("Session Destroyed");
        try {
            ConnectionPool.getInstance().destroy();
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        }
    }
}
