package by.bsu.company.user;


import java.util.Objects;

public class User {
    private int id;
    private String login;
    private String password;
    private boolean isAdmin=false;
    public User(){
        id=0;
        login="login";
        password="****";
    }
    public User(int id,String login, String password){
        this.id=id;
        this.login=login;
        this.password=password;
    }
    public void setAdmin(boolean isAdmin){
        this.isAdmin=isAdmin;
    }
    public boolean isAdmin(){
        return isAdmin;
    }
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }
    public String getLogin(){
        return login;
    }
    public void setLogin(String login){
        this.login=login;
    }
    public void setPassword(String password){
        this.password=password;
    }
    public String getPassword(){
        return password;
    }
    @Override
    public String toString(){
        return "User [id="+id+", login="+login+", password="+password+']';
    }
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        User rhs = (User) obj;
        return id == rhs.id &&
                (Objects.equals(login, rhs.getLogin()) || (login != null && login.equals(rhs.getLogin()))) &&
                (Objects.equals(password, rhs.getPassword()) || (password != null && password.equals(rhs.getPassword()))) &&
                isAdmin==rhs.isAdmin();
    }
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 31 * hash + id;
        hash = 31 * hash + (login == null? 0 : login.hashCode());
        hash = 31 * hash + (password == null?  0 : password.hashCode());
        hash = 31 * hash + (isAdmin ? 1231 : 1237);
        return hash;
    }
}
