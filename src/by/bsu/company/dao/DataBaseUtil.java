package by.bsu.company.dao;

import by.bsu.company.connection.ConnectionPool;
import by.bsu.company.exceptions.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DataBaseUtil {
    private static Logger logger = Logger.getLogger(DataBaseUtil.class);
    private ConnectionPool connection=ConnectionPool.getInstance();
    private Connection cdb;
    public DataBaseUtil() {
        try {
            cdb = connection.retrieve();
        }catch (ConnectionPoolException e){
            logger.error("ConnectionPoolException: error while retrieving connection");
        }
    }
    public Statement getConnectorStatement(){
        Statement st=null;
        try {
            st= cdb.createStatement();
        }catch(SQLException e){
            logger.error("SQLException: error while getting (creating) statement");
        }
        return st;
    }
    public void closePreparedStatement(PreparedStatement ps){
        if(ps!=null) {
            try {
                ps.close();
            } catch (SQLException e) {
                logger.debug("can't close prepared statement");
            }
        }
    }
    public void closeConnectorConnection(){
        try {
            connection.putback(cdb);
        }catch (ConnectionPoolException e){
            logger.error("ConnectionPoolException: error while putting connection back");
        }
    }
    public PreparedStatement getPreparedStatement(String expression) {
        PreparedStatement ps = null;
        try {
            ps = cdb.prepareStatement(expression);
            logger.debug(expression+" statement is prepared");
        } catch (SQLException e) {
            logger.error("SQLException: error while preparing statement");
        }
        return ps;
    }

}
