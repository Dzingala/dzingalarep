package by.bsu.company.dao;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class AddResourcesDAO {
    private final static String ADD_RES_QUERY="UPDATE users SET cash=cash+? WHERE login=?";
    private final static String WITHDRAW_FROM_CARD_QUERY="UPDATE usercards SET cash=cash-? WHERE number=? AND user_id=(SELECT id FROM users WHERE login=?)";
    private final static String GET_USER_CARD_QUERY="SELECT usercards.cash,usercards.status FROM usercards INNER JOIN users ON users.id=usercards.user_id WHERE users.login=? AND usercards.number=?";
    private static Logger logger = Logger.getLogger(AddResourcesDAO.class);
    public static boolean addResources(String login,int price,String cardNumber){
        DataBaseUtil addResObj=null;
        PreparedStatement psAddRes = null;
        PreparedStatement psWithdraw=null;
        PreparedStatement psGetCard=null;
        try{
            addResObj=new DataBaseUtil();

            psGetCard=addResObj.getPreparedStatement(GET_USER_CARD_QUERY);
            psGetCard.setString(1,login);
            psGetCard.setString(2,cardNumber);
            ResultSet rsGetCard =psGetCard.executeQuery();
            int cardCash;
            boolean cardStatus;
            if(rsGetCard.next()){
                cardCash=rsGetCard.getInt("cash");
                cardStatus=rsGetCard.getBoolean("status");
            }
            else{
                logger.debug("no such card");
                return false;
            }

            logger.debug("Got status "+cardStatus+" of card number "+cardNumber+" with cash: "+cardCash);
            if(!cardStatus){
                logger.debug("card number "+cardNumber+" of user "+login+" is unavailable");
                return false;
            }
            if(cardCash<price){
                logger.debug("here is not enough money on card number "+cardNumber+" of user "+login);
                return false;
            }

            psAddRes=addResObj.getPreparedStatement(ADD_RES_QUERY);
            psAddRes.setInt(1, price);
            psAddRes.setString(2, login);
            psAddRes.executeUpdate();
            logger.debug("resources added on the account");

            psWithdraw=addResObj.getPreparedStatement(WITHDRAW_FROM_CARD_QUERY);
            psWithdraw.setInt(1,price);
            psWithdraw.setString(2,cardNumber);
            psWithdraw.setString(3,login);
            psWithdraw.executeUpdate();
            logger.debug("resources subtracted from card");
            addResObj.closePreparedStatement(psAddRes);
            addResObj.closePreparedStatement(psGetCard);
            addResObj.closePreparedStatement(psWithdraw);
        } catch (SQLException e) {
            logger.error("SQLException in AddResourcesDAO.addResources()");
            return false;
        }finally {
            if(addResObj!=null){
                addResObj.closeConnectorConnection();
            }
        }
        return true;
    }

}
