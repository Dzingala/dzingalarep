package by.bsu.company.dao;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConfirmOrdersDAO {
    private final static String CONFIRM_ALL_ORDERS_QUERY="UPDATE orders SET status = true";
    private final static String CONFIRM_ORDERS_QUERY="UPDATE orders INNER JOIN users ON users.id=orders.user_id " +
            " SET orders.status = true WHERE (users.login = ? AND orders.number = ?)";
    private static Logger logger = Logger.getLogger(ConfirmOrdersDAO.class);
    public static boolean confirmOrder(String login,int orderNumber){
        logger.debug("executing ConformOrdersDAO.confirmOrder()");
        DataBaseUtil getUnconfirmed=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            getUnconfirmed= new DataBaseUtil();
            ps = getUnconfirmed.getPreparedStatement(CONFIRM_ORDERS_QUERY);
            ps.setString(1,login);
            ps.setInt(2,orderNumber);
            int bool = ps.executeUpdate();
            if(bool!=0){
                logger.debug("ps executeUpdate not null");
            }
            ps.close();
        }catch (SQLException e){
            logger.error("SQL Exception while executing query in ConfirmOrdersLogic.");

        }finally {
            if (getUnconfirmed != null) {
                getUnconfirmed.closeConnectorConnection();
            }
        }
        return true;
    }
}
