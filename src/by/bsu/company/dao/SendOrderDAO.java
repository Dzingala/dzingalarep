package by.bsu.company.dao;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;


public class SendOrderDAO {
    private final static String INSERT_ORDER_QUERY="INSERT INTO orders (user_id,dish_id,status,number,amount) VALUES (?,?,?,?,?) ";
    private final static String GET_DISH_ID="SELECT dish_id,dish FROM menu";
    private final static String GET_USER_ID="SELECT id FROM users WHERE login=?";
    private final static String GET_ORDER_NUMBER_QUERY= "SELECT * FROM orders WHERE number = " +
            "(SELECT MAX(number) FROM orders WHERE user_id=?) ";
    private final static String UPDATE_USER_CASH_QUERY="UPDATE users SET cash=? WHERE login=?";
    private static Logger logger = Logger.getLogger(SendOrderDAO.class);
    public static boolean sendOrder(Map<String,int[]> order,String login, int updatedUserCash){
        logger.debug("executing SendOrderLogic.sendOrder()");
        DataBaseUtil orderManager=null;
        PreparedStatement psUpdCash=null;
        PreparedStatement psInsertOrd= null;
        PreparedStatement psGetOrdNum = null;
        Statement stGetDishes = null;
        PreparedStatement psGetLoginId = null;
        try{
            orderManager = new DataBaseUtil();
            stGetDishes = orderManager.getConnectorStatement();

            ResultSet rsGetOrdNum;
            ResultSet rsGetLoginId=null;
            ResultSet rs = null;
            rs =stGetDishes.executeQuery(GET_DISH_ID);

            psGetLoginId = orderManager.getPreparedStatement(GET_USER_ID);
            psGetLoginId.setString(1,login);

            psUpdCash= orderManager.getPreparedStatement(UPDATE_USER_CASH_QUERY);
            psUpdCash.setInt(1,updatedUserCash);
            psUpdCash.setString(2,login);

            rsGetLoginId=psGetLoginId.executeQuery();
            int loginId=0;
            if(rsGetLoginId.next()){
                loginId = rsGetLoginId.getInt("id");
            }

            psUpdCash.executeUpdate();

            psGetOrdNum=orderManager.getPreparedStatement(GET_ORDER_NUMBER_QUERY);
            psGetOrdNum.setInt(1,loginId);
            rsGetOrdNum=psGetOrdNum.executeQuery();
            int orderNumber;
            int currentOrderNumber=1000;
            if(rsGetOrdNum.next()) {
                orderNumber = rsGetOrdNum.getInt("number");
                currentOrderNumber = orderNumber + 1;
                logger.debug("at least 1 order for login "+login+" exists, so order number = "+currentOrderNumber);
            }
            while(rs.next()) {
                String dish = rs.getString("dish");
                int dish_id = rs.getInt("dish_id");
                for (Map.Entry<String, int[]> entry : order.entrySet()) {
                    logger.debug("loop over treemap");
                    if(dish.equals(entry.getKey())) {
                        psInsertOrd = orderManager.getPreparedStatement(INSERT_ORDER_QUERY);
                        try {
                            psInsertOrd.setInt(1, loginId);
                            psInsertOrd.setInt(2, dish_id);
                            psInsertOrd.setBoolean(3,false);
                            psInsertOrd.setInt(4, currentOrderNumber);
                            psInsertOrd.setInt(5, entry.getValue()[1]);
                            psInsertOrd.executeUpdate();
                        } catch (SQLException e) {
                            logger.error("SQL Exception in SendOrderLogic while inserting");
                        }
                    }
                }
            }
            orderManager.closePreparedStatement(psGetLoginId);
            orderManager.closePreparedStatement(psGetOrdNum);
            orderManager.closePreparedStatement(psInsertOrd);
            orderManager.closePreparedStatement(psUpdCash);
        }catch (SQLException e){
            logger.error("SQL Exception in SendOrderLogic.");
        }finally{
            if (orderManager != null) {
                orderManager.closeConnectorConnection();
            }
            logger.debug("Connection is closed");
        }
        return true;
    }
}
