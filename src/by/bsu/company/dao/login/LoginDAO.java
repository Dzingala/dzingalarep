package by.bsu.company.dao.login;

import by.bsu.company.dao.DataBaseUtil;
import by.bsu.company.exceptions.LogicException;
import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class LoginDAO {

    private final static String GET_LOGIN_ORDERS_QUERY=
            "SELECT menu.dish, menu.price, orders.status, orders.number,orders.amount FROM users INNER JOIN (menu INNER JOIN  orders ON menu.dish_id=orders.dish_id) ON users.id=orders.user_id" +
                    " WHERE users.login = ?";
    private final static String GET_LOGIN_ACCESS_QUERY="SELECT accessStatus FROM users WHERE login=?";
    private static Logger logger = Logger.getLogger(LoginDAO.class);
    public static boolean checkLoginAccess(String login){
        boolean access= true;
        DataBaseUtil checkLogin=null;
        PreparedStatement psLoginAccess=null;
        try{
            checkLogin=new DataBaseUtil();
            ResultSet rsAccess=null;
            psLoginAccess= checkLogin.getPreparedStatement(GET_LOGIN_ACCESS_QUERY);
            psLoginAccess.setString(1,login);
            rsAccess=psLoginAccess.executeQuery();
            if(rsAccess.next()){
                access=rsAccess.getBoolean("accessStatus");
            }else return false;
        } catch (SQLException e) {
            logger.error("SQLException in LoginDAO.checkLoginAccess()");
        }finally {
            if(checkLogin!=null){
                checkLogin.closeConnectorConnection();
                logger.debug("Connection is closed in checkLoginAccess");
            }
        }
        return access;
    }
    public static Map<Integer,ArrayList<String>>  getLoginOrders(String login){
        logger.debug("getLoginOrders() called");
        Map<Integer,ArrayList<String>> orders = new TreeMap<>();
        DataBaseUtil getOrders=null;
        PreparedStatement ps = null;
        try {
            getOrders = new DataBaseUtil();
            ps = getOrders.getPreparedStatement(GET_LOGIN_ORDERS_QUERY);
            ps.setString(1, login);
            ResultSet rs = null;
            try {
                rs = ps.executeQuery();

                while(rs.next()){
                    String dish = rs.getString("menu.dish");
                    int amount = rs.getInt("orders.amount");
                    int price=rs.getInt("menu.price");
                    int orderNumber=rs.getInt("orders.number");
                    boolean status = rs.getBoolean("orders.status");
                    ArrayList<String> price_status= null;
                    if(orders.get(orderNumber)==null){
                        logger.debug("orders are null for "+login+", initialization");
                        price_status= new ArrayList<>();
                    }
                    else{
                        logger.debug("orders are not null for "+login+", adding new dishes");
                        price_status = orders.get(orderNumber);
                    }
                    price_status.add(dish);
                    price_status.add(String.valueOf(price));
                    price_status.add(String.valueOf(amount));
                    price_status.add(status? LocaleManager.getProperty("message.confirmed"):LocaleManager.getProperty("message.unconfirmed"));
                    orders.put(orderNumber,price_status);
                }
            }catch(LogicException e){
                logger.error("SQLException in LoginLogic.getLoginOrders().");
                e.printStackTrace();
            }
        }catch (SQLException e){
            logger.error("SQLException while getting login orders in LoginLogic.getLoginOrders().");
            e.printStackTrace();
        }finally {
            if (getOrders != null) {
                getOrders.closeConnectorConnection();
            }
        }
        return orders;
    }

}
