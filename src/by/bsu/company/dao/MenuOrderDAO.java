package by.bsu.company.dao;


import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class MenuOrderDAO {
    public final static String GET_MENU_QUERY="SELECT * FROM menu WHERE isAvailable = 1";
    public final static String GET_ADMIN_MENU_QUERY="SELECT * FROM menu";
    private static Logger logger = Logger.getLogger(MenuOrderDAO.class);
    public static Map<String,String[]> getUserMenu(){
        logger.debug("executing MenuOrderDAO.getUserMenu()");
       //операции взятия элемента по ключу, вставка или удаления происходят за O(logN).
        Map<String, String[]> menu = new TreeMap<>();

        DataBaseUtil calculateCost=null;
        Statement st = null;
        try{
            calculateCost=new DataBaseUtil();
            st = calculateCost.getConnectorStatement();
            ResultSet rs =st.executeQuery(GET_MENU_QUERY);
            while(rs.next()){
                String dish = rs.getString("dish");
                int price = rs.getInt("price");
                boolean isAvailable = rs.getBoolean("isAvailable");
                String isAvailableString = isAvailable? LocaleManager.getProperty("message.available"):LocaleManager.getProperty("message.unavailable");
                String price_isAvailable[]= new String[2];
                price_isAvailable[0]=String.valueOf(price);
                price_isAvailable[1]=isAvailableString;
                menu.put(dish,price_isAvailable);
            }
        }catch (SQLException e){
            logger.error("SQL Exception (statement getting failed)");
        }finally {
            if (calculateCost != null) {
                calculateCost.closeConnectorConnection();
            }
            logger.debug("Connection is closed");
        }
        return menu;
    }
    public static Map<String,String[]> getAdminMenu(){
        logger.debug("executing MenuOrderDAO.getAdminMenu()");
        //operations of taking by the key, inserting or deleting performs for O(logN).
        Map<String, String[]> menu = new TreeMap<>();

        DataBaseUtil calculateCost=null;
        Statement st = null;
        try{
            calculateCost=new DataBaseUtil();
            st = calculateCost.getConnectorStatement();
            ResultSet rs =st.executeQuery(GET_ADMIN_MENU_QUERY);
            while(rs.next()){
                String dish = rs.getString("dish");
                int price = rs.getInt("price");
                boolean isAvailable = rs.getBoolean("isAvailable");
                String isAvailableString = isAvailable? LocaleManager.getProperty("message.available"):LocaleManager.getProperty("message.unavailable");
                String price_isAvailable[]= new String[3];
                price_isAvailable[0]=String.valueOf(price);
                price_isAvailable[1]=isAvailableString;
                price_isAvailable[2]=isAvailable?LocaleManager.getProperty("message.deleteDish"):LocaleManager.getProperty("message.enableDish");
                menu.put(dish,price_isAvailable);
            }
            st.close();
        }catch (SQLException e){
            logger.error("SQL Exception (statement getting failed)");
        }finally {
            if (calculateCost != null) {
                calculateCost.closeConnectorConnection();
            }
            logger.debug("Connection is closed");
        }
        return menu;
    }
}

