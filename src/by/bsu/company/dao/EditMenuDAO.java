package by.bsu.company.dao;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class EditMenuDAO {
    private final static String ADD_DISH_QUERY="INSERT INTO menu(dish,price) VALUES(?,?)";
    private final static String DELETE_DISH_QUERY="UPDATE menu SET isAvailable=? WHERE menu.dish=?";
    private final static String DOES_EXISTS_IN_UNCONFIRMED_DISH="SELECT menu.dish FROM orders " +
            "INNER JOIN menu ON orders.dish_id = menu.dish_id WHERE orders.status = 0 ";
    private static Logger logger = Logger.getLogger(EditMenuDAO.class);
    public static boolean addDish(String name, int price){
        Map<String,String[]> menu= MenuOrderDAO.getAdminMenu();
        for (Map.Entry<String,String[]> entry: menu.entrySet()) {
            if((name.trim()).equals(entry.getKey())){
                logger.debug("such dish already exists");
                return false;
            }
        }
        DataBaseUtil addDish=null;
        PreparedStatement ps=null;
        try {
            addDish = new DataBaseUtil();
            ps = addDish.getPreparedStatement(ADD_DISH_QUERY);
            ps.setString(1,name.trim());
            ps.setInt(2,price);
            ps.executeUpdate();
            addDish.closePreparedStatement(ps);
        }catch (SQLException e){
            logger.error("SQLException in EditMenuLogic.addDish()");
        }finally {
            if (addDish != null) {
                addDish.closeConnectorConnection();
            }
        }
        return true;
    }

    public static boolean editDishStatus(String name,boolean status){
        logger.debug("Executing EditMenuDAO.disableDish()");
        logger.debug("name of dish came:"+name);
        Map<String,String[]> menu= MenuOrderDAO.getAdminMenu();
        boolean exists=false;
        for (Map.Entry<String,String[]> entry: menu.entrySet()) {
            if((name.trim()).equals(entry.getKey())){
                exists=true;
                break;
            }
        }
        if(!exists)return false;
        DataBaseUtil delDish=null;
        PreparedStatement ps=null;
        try {
            delDish = new DataBaseUtil();
            ps = delDish.getPreparedStatement(DELETE_DISH_QUERY);
            logger.debug("name to set:"+name);
            ps.setBoolean(1,status);
            ps.setString(2,name);
            ps.executeUpdate();
            delDish.closePreparedStatement(ps);
        }catch (SQLException e){
            logger.error("SQLException in EditMenuLogic.deleteDish()");
        }
        finally {
            if (delDish != null) {
                delDish.closeConnectorConnection();
            }
        }
        return true;
    }
}
