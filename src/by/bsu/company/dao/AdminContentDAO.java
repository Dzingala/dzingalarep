package by.bsu.company.dao;

import by.bsu.company.manager.LocaleManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class AdminContentDAO {
    private final static String GET_UNCONFIRMED_ORDERS_QUERY="SELECT " +
            " users.login, menu.dish, menu.price, orders.amount, orders.number " +
            " FROM users INNER JOIN" +
            " (menu INNER JOIN orders ON menu.dish_id =orders.dish_id)" +
            " ON users.id = orders.user_id\n" +
            " WHERE status = FALSE ";
    private final static String GET_USERS_QUERY = "SELECT login,cash,accessStatus FROM users WHERE isAdmin=false";
    private final static String BAN_USER_QUERY="UPDATE users SET accessStatus=? WHERE login=?";
    private static Logger logger = Logger.getLogger(AdminContentDAO.class);

    public static boolean banUser(String login,boolean status){
        DataBaseUtil banUserObj=null;
        PreparedStatement ps = null;
        try{
            banUserObj=new DataBaseUtil();
            ps=banUserObj.getPreparedStatement(BAN_USER_QUERY);
            ps.setBoolean(1, status);
            ps.setString(2, login);
            logger.debug("prepared for ban:"+ps);
            ps.executeUpdate();
            banUserObj.closePreparedStatement(ps);
        } catch (SQLException e) {
            logger.error("SQLException in AdminContentDAO.banUser()");
        }finally {
            if(banUserObj!=null){
                banUserObj.closeConnectorConnection();
            }
        }
        return true;
    }
    public static Map<String,String[]> getUsersList(){
        Map<String,String[]> users = new TreeMap<>();
        DataBaseUtil getUsers=null;
        Statement st = null;
        ResultSet rs=null;
        try{
            getUsers=new DataBaseUtil();
            st = getUsers.getConnectorStatement();
            rs=st.executeQuery(GET_USERS_QUERY);
            while(rs.next()){
                String[] cash_access = new String[3];
                cash_access[0]=(String.valueOf(rs.getInt("cash")));
                boolean currentAccess=rs.getBoolean("accessStatus");
                cash_access[1]=(currentAccess? LocaleManager.getProperty("message.notBanned"): LocaleManager.getProperty("message.banned"));
                cash_access[2]=(currentAccess?LocaleManager.getProperty("message.banButton"):LocaleManager.getProperty("message.unbanButton"));
                users.put(rs.getString("login"),cash_access);
            }
            st.close();
        } catch (SQLException e) {
            logger.error("SQLException in AdminContentDAO.getUsersList()");
        }finally {
            if(getUsers!=null){
                getUsers.closeConnectorConnection();
            }
        }
        return users;
    }
    public static Map<String,Map<Integer,Integer>> getUnconfirmedOrders(){
        Map<String,Map<Integer,Integer>> orders = null;
        DataBaseUtil getUnconfirmed=null;
        Statement st = null;
        ResultSet rs=null;
        try{
            orders = new TreeMap<>();
            getUnconfirmed= new DataBaseUtil();
            st = getUnconfirmed.getConnectorStatement();
            rs= st.executeQuery(GET_UNCONFIRMED_ORDERS_QUERY);
            String login;
            int price;
            int orderNumber;
            int amount;
            String dish;
            while(rs.next()){
                amount=rs.getInt("amount");
                dish = rs.getString("dish");
                login=rs.getString("login");
                price=rs.getInt("price");
                orderNumber=rs.getInt("number");
                int dishTotalPrice= amount * price;
                logger.debug("got: login="+login+", price="+price+", ordNum="+orderNumber+", amount="+amount+", dish="+dish);
                if(orders.get(login)==null){
                    System.out.println("Login = null");
                    Map<Integer,Integer> num_price = new TreeMap<>();
                    num_price.put(orderNumber,dishTotalPrice);
                    logger.debug("put NEW ORD: "+ login +", ord:"+orderNumber+", price:" + dishTotalPrice);
                    orders.put(login,num_price);
                }
                else{
                    Map<Integer,Integer> num_price=orders.get(login);

                    System.out.println("else entered");
                    if(num_price.get(orderNumber)==null){
                        logger.debug("orderNumber = null");
                        num_price.put(orderNumber,dishTotalPrice);
                    }
                    else{
                        logger.debug("orders for login exists, such order number exists, => adding total price");
                        int orderTotalPrice = num_price.get(orderNumber);
                        orderTotalPrice+=dishTotalPrice;
                        num_price.put(orderNumber,orderTotalPrice);
                    }
                    orders.put(login,num_price);
                }
            }
            st.close();
        }catch (SQLException e){
            logger.error("SQLException while getting unconfirmed orders in AdminLogic.getUnconfirmedOrders();");
        }finally {
            if (getUnconfirmed != null) {
                getUnconfirmed.closeConnectorConnection();
            }
        }
        return orders;
    }
}
