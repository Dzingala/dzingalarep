package by.bsu.company.dao;

import by.bsu.company.md5util.MD5Hash;
import org.apache.log4j.Logger;

import java.sql.*;

public class RegistrationDAO {
    private final static String SQL_INSERT = "INSERT INTO users(login, password,isAdmin,cash,accessStatus) VALUES(?,?,?,?,?)";
    private final static String QUERY="SELECT * FROM users WHERE login=?";
    private static Logger logger = Logger.getLogger(RegistrationDAO.class);
    public static boolean checkLogin(String enterLogin, String enterPass){
        logger.debug("executing RegistrationDAO.checkLogin()");
        if(enterLogin.equals("") || enterPass.equals("")){
            logger.debug("empty login or password");
            return false;
        }
        if(enterPass.length()<4
                || enterPass.equals(enterPass.toLowerCase())
                || !enterPass.matches("[A-Za-z0-9 ]*")
                || enterLogin.length()<4
                || enterLogin.equals(enterLogin.toUpperCase())){
            logger.debug("login or password mismatch");
            return false;
        }
        boolean exists=false;
        DataBaseUtil adduser=null;
        PreparedStatement ps=null;
        PreparedStatement preparedStatement=null;

        try {
            adduser = new DataBaseUtil();

            ps = adduser.getPreparedStatement(SQL_INSERT);
            preparedStatement = adduser.getPreparedStatement(QUERY);
            preparedStatement.setString(1, enterLogin);
            ResultSet rs = null;
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                logger.debug("such login already exists");
                exists = true;
            }
            if (!exists) {
                addUser(ps,enterLogin,MD5Hash.md5(enterPass));
            }
            if(ps!=null) {
                adduser.closePreparedStatement(ps);
            }
            adduser.closePreparedStatement(preparedStatement);
        }catch (SQLException e){
            logger.error("Error connection database");
        }finally{
            if (adduser != null) {
                adduser.closeConnectorConnection();
            }
            logger.debug("Connection is closed");
        }
        return !exists;
    }
    static boolean addUser(PreparedStatement ps,String login, String password){
        logger.debug("RegistrationDAO.addUser() is executing");
        logger.debug("Statement came: "+ps);
        logger.debug("Login came: "+login+", Password came: "+password);
        boolean flag=false;
        try{
            ps.setString(1, login);
            ps.setString(2,password);
            ps.setBoolean(3, false);
            ps.setInt(4, 0);
            ps.setBoolean(5,true);
            logger.debug("sets are made successful. ps: "+ps);
            ps.executeUpdate();
            flag =true;
        }catch (SQLException e){
            logger.error("error in adding user");
        }
        return flag;
    }
}
