package by.bsu.company.logic;

import org.apache.log4j.Logger;


public class ChangeMenuLogic {
    private static Logger logger = Logger.getLogger(ChangeMenuLogic.class);
    public static boolean checkPrice(String price){
        int numPrice;
        try{
            numPrice=Integer.parseInt(price);
            if(numPrice<=99 || numPrice >100000)return false;
        }catch(NumberFormatException e){
            logger.error("incorrect price value.");
            return false;
        }
        return true;
    }
}
