package by.bsu.company.logic;

import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AddResourcesLogic {
    private static Logger logger = Logger.getLogger(AddResourcesLogic.class);
    public static boolean checkPrice(String price){
        logger.debug("Checking price in AddResourcesLogic");
        int intPrice;
        try{
            intPrice=Integer.parseInt(price);
            if(intPrice<100 || intPrice>100000){
                return false;
            }
        }catch (NumberFormatException e){
            logger.error("NumberFormatException in checkPrice()");
            return false;
        }
        return true;
    }
    public static boolean checkCardNumber(String cardNumber){
        logger.debug("Checking card number in AddResourcesLogic");
        logger.debug("number came: "+cardNumber);
        if(cardNumber.length()!=16){
            logger.debug("card number contains not 16 symbols, stop.");
            return false;
        }
        if(!cardNumber.matches("[0-9]+")) {
            logger.error("NumberFormatException in checkCardNumber");
            return false;
        }
        return true;
    }
}
