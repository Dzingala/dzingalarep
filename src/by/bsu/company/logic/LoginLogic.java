package by.bsu.company.logic;

import by.bsu.company.dao.DataBaseUtil;
import by.bsu.company.md5util.MD5Hash;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginLogic {
    private final static String QUERY = "SELECT * FROM users WHERE login=?";
    private final static String GET_CASH_QUERY = "SELECT cash FROM users WHERE login=?";
    private static Logger logger = Logger.getLogger(LoginLogic.class);
    private static boolean isAdmin=false;

    public static boolean checkLogin(String enterLogin, String enterPass/*, User isAdmin*/) {
        logger.debug("LoginLogic.ckeckLogin() is called");
        if (enterLogin.equals("") || enterPass.equals("")) {
            return false;
        }
        String login;
        String password;
        int id;
        boolean admin;
        boolean isLogin = false;
        DataBaseUtil loginObject = null;
        PreparedStatement ps = null;
        try {
            loginObject = new DataBaseUtil();
            ps = loginObject.getPreparedStatement(QUERY);
            ps.setString(1, enterLogin);
            ResultSet rs = null;

            rs = ps.executeQuery();

            if (rs.next()) {
                logger.debug("user exists:");
                login = rs.getString("login");
                id = rs.getInt("id");
                password = rs.getString("password");
                admin = rs.getBoolean("isAdmin");
                logger.debug("login=" + login + ", id=" + id + ", password=" + password + ", admin=" + admin);
            } else {
                logger.debug("no such login in checkLogin()");
                return false;
            }
            isAdmin=admin;

            if (password.equals(MD5Hash.md5(enterPass))) {
                isLogin = true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            logger.error("Error connection database in LoginLogic.checkLogin()");
        } finally {
            if (loginObject != null) {
                loginObject.closeConnectorConnection();
                logger.debug("Connection is closed in LoginLogic.checkLogin()");
            }

        }
        return isLogin;
    }
    public static boolean isAdmin(){
        return isAdmin;
    }
    public static int getUserCash(String enterLogin) {
        logger.debug("LoginLogic.getUserCash() is called");
        int userCash = 0;
        DataBaseUtil getCashObject = null;
        PreparedStatement ps = null;
        try {
            getCashObject = new DataBaseUtil();
            ps = getCashObject.getPreparedStatement(GET_CASH_QUERY);
            ps.setString(1, enterLogin);
            ResultSet rs = null;

            rs=ps.executeQuery();
            if(rs.next()){
                logger.debug("user exists");
                userCash=rs.getInt("cash");
                logger.debug("Cash for user "+enterLogin+": "+userCash);
            }
            else{
                logger.error("no such user");
                return -1;
            }
        } catch (SQLException e) {
            logger.error("SQLException in LoginLogic.getUserCash");
        }finally {
            if(getCashObject!=null) {
                getCashObject.closeConnectorConnection();
                logger.debug("Connection is closed in LoginLogic.getUserCash()");
            }
        }
        return userCash;
    }
}
