package by.bsu.company.logic;


import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Objects;

public class OrderLogic {
    private static Logger logger = Logger.getLogger(OrderLogic.class);
    public static int matchOrderWithCash(int totalPrice, int userCash){
        return userCash-totalPrice;
    }
    public static boolean checkOrderValues(String[] checkboxValues, String[] amounts,String[] dishes){
        if(checkboxValues == null || amounts==null || dishes == null || amounts.length!=dishes.length)return false;
        logger.debug("OrderLogic.checkOrderValues() is called, input parameters are right.");
        for (String amount : amounts) {
            if (!Objects.equals(amount, "")) {
                try {
                    int k = Integer.parseInt(amount);
                    if (k <= 0 || k >= 100) {
                        logger.error("Illegal number value in amounts of dishes while ordering.");
                        return false;
                    }
                } catch (NumberFormatException e) {
                    logger.error("Illegal number format in amounts of dishes while ordering.");
                    return false;
                }
            }
        }
        return true;
    }

    public static int setOrder(Map<String, int[]> order,Map<String,String[]> menu,String[] checkboxValues,String[] amounts){
        logger.debug("OrderLogic.setOrder() is called");
        int totalPrice = 0;
        int it = 0;
        for (Map.Entry<String, String[]> entry : menu.entrySet()) {
            String dish = entry.getKey(); // getting the key
            int price = Integer.parseInt(entry.getValue()[0]); // getting the value
            for (String checkboxValue : checkboxValues) {
                if (checkboxValue.equals(dish)) {
                    int amount;
                    try{
                        amount=Integer.parseInt(amounts[it]);
                    }catch (NumberFormatException e){
                        logger.error("no dishes amount input or is wrong");
                        return -1;
                    }
                    totalPrice += price*amount;
                    logger.debug("total price for "+dish+"* "+amounts[it] +" = "+totalPrice);
                    int price_amount[] = new int[2];
                    price_amount[0]= totalPrice;
                    price_amount[1]=amount;
                    order.put(dish, price_amount);
                    totalPrice=0;
                    logger.debug("Added: " + dish + ", costs: " + price+", available?:" + /*(isAvailable?*/"yes"/*:"no")*/);
                }
            }
            it++;
        }
        for (Map.Entry<String, int[]> entry : order.entrySet()) {
            totalPrice+=entry.getValue()[0];
        }
        return totalPrice;
    }
}
