<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fctg" uri="customfooter" %>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.message" var="loc"/>
<html>
<head>
    <style>
        body{
            text-align: center;
            margin:0 auto;
        }
        table {
            border-spacing: 0 10px;
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            margin:auto;
        }
        th {
            padding: 10px 20px;
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid;
            font-size: 0.9em;
        }
        th:first-child {
            text-align: left;
        }
        th:last-child {
            border-right: none;
        }
        td {
            vertical-align: middle;
            padding: 10px;
            font-size: 14px;
            text-align: center;
            border-top: 2px solid #56433D;
            border-bottom: 2px solid #56433D;
            border-right: 2px solid #56433D;
        }
        td:first-child {
            border-left: 2px solid #56433D;
            border-right: none;
        }
        td:nth-child(2){
            text-align: left;
        }
        form{
            margin-top:15px;
        }
        #exit{
            position: absolute;
            margin-left: 450px;
            color:white;
        }
        #exit:hover{
            background: white;
            color:black;
        }
        #top{
            background: url(/img/bgr.jpg) no-repeat center center;
            background-attachment:fixed;
            background-size:cover;
            height: 300px;
        }
        #top h3{
            background: #000000;
        }
        ul {
            list-style-type: none;
            margin: 0;
            margin-top:-10px;
            padding: 0;
            width: 15%;
            background-color: white;
            position: fixed;
            height: 25%;
            overflow: auto;
        }

        li a {
            display: block;
            color: #000;
            padding: 8px 0 8px 16px;
            text-decoration: none;
        }

        li a.active {
            background-color: #56433D;
            color: #F9C941;
        }

        li a:hover:not(.active) {
            background-color: #555;
            color: white;
        }
    </style>
    <title></title>
</head>
<body>
<div id = "top">
    <h3 style="color:white;">${user} <fmt:message key="message.identified" bundle="${loc}"/><a id = "exit" href="controller?command=logout"><fmt:message key="message.exitLink" bundle="${loc}"/></a>
    </h3>
    <br/>
</div>
<hr/>
<ul>
    <li><a href="controller?command=banlink"><fmt:message key="message.banlink" bundle="${loc}"/></a></li>
    <li><a href="controller?command=addlink"><fmt:message key="message.addlink" bundle="${loc}"/></a></li>
    <li><a class="active" href="controller?command=confirmlink"><fmt:message key="message.confirmlink" bundle="${loc}"/></a></li>
</ul>
<br/>
<fmt:message key="message.unconfirmedOrdersMessage" bundle="${loc}"/>:
<br/>
<table>
    <tr>
        <th><fmt:message key="message.login" bundle="${loc}"/></th>
        <th><fmt:message key="message.orderNumber" bundle="${loc}"/></th>
        <th><fmt:message key="message.totalPrice" bundle="${loc}"/></th>
    </tr>
    <c:forEach var ="map" items="${unconfirmedOrders}" >
        <c:forEach var ="map2" items="${map.value}" >
            <tr>
                <td>${map.key}</td>
                <td>${map2.key}</td>
                <td>${map2.value}</td>
                <td><form name = "confirmButton" method="POST" action="controller" >
                    <input type="hidden" name="command" value="confirm"/>
                    <input type="hidden" name="loginToConfirm" value ="${map.key}"/>
                    <input type="hidden" name="orderNumberToConfirm" value ="${map2.key}"/>
                    <input type="submit" value="${singleConfirmButton}"><br>
                </form>
                </td>
            </tr>
        </c:forEach>
    </c:forEach>
</table>
<br/>
${confirmSuccessMessage}${confirmFailMessage}
<br/>
<br/>
</body>
<fctg:copyrightInfoTag/>
</html>
