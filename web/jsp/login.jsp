<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.message" var="loc"/>
<fmt:requestEncoding value="UTF-8" />

<html>

<head>
    <link href="style/menustyle.css" type="text/css" rel="stylesheet">
    <title><fmt:message key="message.title" bundle="${loc}"/></title>
</head>
<body>
    <section class = "container">
    <div class = "login">
        <h1><fmt:message key="message.head" bundle="${loc}"/> </h1>
        <form name="loginForm" method="POST" action="controller">
            <input type="hidden" name="command" value="login" />
            <p>
                <label>
                    <fmt:message key="message.login" bundle="${loc}"/>:<br/>
                    <input type="text"
                        name="login"
                        value=""
                        pattern="(?=.*[a-z]).{4,}"
                        required
                        title="<fmt:message key="message.loginTitle" bundle="${loc}"/>"
                        />
                </label>
            </p>
            <p>
                <label>
                    <br/><fmt:message key="message.password" bundle="${loc}"/>:<br/>
                    <input type="password"
                        name="password"
                        value=""
                        title="<fmt:message key="message.passWarning" bundle="${loc}"/>"
                        />
                </label>
            </p>
            <br/> ${errorLoginPassMessage}
            <br/> ${wrongAction}
            <br/> ${nullPage}
            <br/>
            <p class = "submit">
                <input type="submit" value="<fmt:message key="message.loginButton" bundle="${loc}"/>"/>
            </p>
        </form>
    </div>
    <br/>
    <fmt:message key="message.noAccount" bundle="${loc}"/> <a href="controller?command=registration"><fmt:message key="message.letsRegister" bundle="${loc}"/></a>
    <hr/>
    <br/>
    <form action="controller" method="post" style="display:inline-block;" >
        <input type="hidden" name="command" value="locale"/>
        <input type="hidden" name="page" value="/jsp/login.jsp"/>
        <input type="hidden" name="lang" value="en"/>
        <input class='button' type="submit" name="command" value="EN"/>
    </form>
    <form action="controller" method="post" style="display:inline-block;" >
        <input type="hidden" name="command" value="locale"/>
        <input type="hidden" name="page" value="/jsp/login.jsp"/>
        <input type="hidden" name="lang" value="ru"/>
        <input class='button' type="submit" name="command" value="RU"/>
    </form>
    <hr/>
    </section>
</body>
</html>

