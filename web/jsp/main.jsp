<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.message" var="loc"/>
<html>
<head>
    <title></title>
    <style>
        *{
            margin:0 auto;
        }
        table {
            border-spacing: 0 10px;
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            margin:auto;
        }
        th {
            padding: 10px 20px;
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid;
            font-size: 0.9em;
        }
        #calculate{
            padding: 10px 20px;
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid;
            font-size: 0.9em;
        }
        th:first-child {
            text-align: left;
        }
        th:last-child {
            border-right: none;
        }
        td {
            vertical-align: middle;
            padding: 10px;
            font-size: 14px;
            text-align: center;
            border-top: 2px solid #56433D;
            border-bottom: 2px solid #56433D;
            border-right: 2px solid #56433D;
        }
        td:first-child {
            border-left: 2px solid #56433D;
            border-right: none;
        }
        td:nth-child(2){
            text-align: left;
        }
        form{
            margin-top:15px;
        }

        #exit{
            position: absolute;
            margin-left: 450px;
            color:white;
        }
        #exit:hover{
            background: white;
            color:black;
        }
        body{
            text-align: center;
            margin:0 auto;
        }
        #top{
            background: url(/img/bgr.jpg) no-repeat center center;
            background-attachment:fixed;
            background-size:cover;
            height: 300px;
        }
        #top h3{
            background: #000000;
        }
        #cash{
            position:relative;
            margin-left: 1000px;
        }
    </style>
</head>
<body>
    <div id = "top">
    <h3 style="color:white;">${user} <fmt:message key="message.identified" bundle="${loc}"/><a id = "exit" href="controller?command=logout"><fmt:message key="message.exitLink" bundle="${loc}"/></a>
    </h3>
    <br/>
    </div>
    <hr/>
    <br/>
    <fmt:message key="message.yourRecentOrders" bundle="${loc}"/><br/>
    <h3 id = "cash">
        <fmt:message key="message.cash" bundle="${loc}"/> ${cash}
        <p>
        <form name="input" action="controller" method="post">
            <input type="hidden" name="command" value="addResources" />
            <label>
                <fmt:message key="message.enterCardNumber" bundle="${loc}"/>
                <input type="text"
                       name="cardNumber"
                       pattern="(?=.[0-9]).{16,}"
                       required
                       title="<fmt:message key="message.enterCardNumberTitle" bundle="${loc}"/>"
                        />
            </label>
            <br/>
            <label>
                <fmt:message key="message.enterAddPrice" bundle="${loc}"/>
                <input type="text"
                       name="moneyAmount"
                       pattern="(?=.[0-9]).{3,}"
                       required
                       title="<fmt:message key="message.priceAddTitle" bundle="${loc}"/>"
                        />
            </label>
            <br/>
            <input type="submit" value="<fmt:message key="message.addResourcesButton" bundle="${loc}"/>"/>
        </form>
        ${addingResourcesStatusMessage}
        </p>
    </h3>
    <table>
        <tr>
            <th> <fmt:message key="message.orderNumber" bundle="${loc}"/></th>
            <th> <fmt:message key="message.dishName" bundle="${loc}"/></th>
            <th> <fmt:message key="message.price" bundle="${loc}"/></th>
            <th> <fmt:message key="message.amount" bundle="${loc}"/></th>
            <th> <fmt:message key="message.orderStatus" bundle="${loc}"/></th>

        </tr>
        <c:forEach var ="map" items="${userOrders}" >
            <tr>
                <td>${map.key}</td>
                <c:forEach items="${map.value}" var="item" varStatus="loop">
                    <td>${item}</td>${item.equals("Confirmed") ||  item.equals("Unconfirmed")|| item.equals("Подтвержден") || item.equals("Не подтвержден")?(!loop.last?"</tr><tr><td></td>":""):""}
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
    <br/>
    <form name="input" action="controller" method="post">
        <input type="hidden" name="command" value="order" />
        <fmt:message key="message.availableDishesListIntro" bundle="${loc}"/><br/>
        <table>
            <tr>
                <th> <fmt:message key="message.isChosen" bundle="${loc}"/></th>
                <th> <fmt:message key="message.dishName" bundle="${loc}"/></th>
                <th> <fmt:message key="message.price" bundle="${loc}"/></th>
                <%--<th> <fmt:message key="message.isAvailable" bundle="${loc}"/> </th>--%>
                <th> <fmt:message key="message.amount" bundle="${loc}"/><fmt:message key="message.enter(table)" bundle="${loc}"/> </th>
            </tr>
            <c:forEach var ="map" items="${list}" varStatus="loop">
                <tr>
                    <input type="hidden" name="menu" value="${map.key}"/>
                    <td><input style="width:100px;" id = "${map.key}" type="checkbox" name="selected" value="${map.key}"/></td>
                    <td><label for = "${map.key}">${map.key}</label></td>
                    <td><label for = "${map.key}">${map.value[0]}</label></td>
                    <td><label for = "${map.key}"><input
                            type="number"
                            min="1"
                            step="1"
                            name="amount"
                            title="<fmt:message key="message.dishAmountTitle" bundle="${loc}"/>"
                            /></label></td>
                </tr>
        </c:forEach>
        </table>
        ${noAmountsMessage}
        ${noneSelectedMessage}
        ${ordersUnavailableMessage}
        <br/><br/><input type="submit" id = "calculate" value=" <fmt:message key="message.calculateButton" bundle="${loc}"/>"><br/><br/>
        ${orderReadyMessage}<br/><br/>
        <c:forEach var ="map" items="${order}" >
            - ${map.key} (${costs} ${map.value[0]} <fmt:message key="message.for" bundle="${loc}"/> ${map.value[1]} <fmt:message key="message.ones" bundle="${loc}"/>.)<br/>
        </c:forEach>
        <br/>
        ${costMessage}<br/>
        <span style="color: red; ">${warningMessage} ${debt}</span>
        <br/>
    </form>
    <br/>
    ${sendButton}${sentMessage}
    <br/>
    <br/>
</body>
</html>
