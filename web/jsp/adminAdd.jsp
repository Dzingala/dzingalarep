<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fctg" uri="customfooter" %>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.message" var="loc"/>
<html>
<head>
    <style>
        body{
            text-align: center;
            margin:0 auto;
        }
        table {
            border-spacing: 0 10px;
            font-family: 'Open Sans', sans-serif;
            font-weight: bold;
            margin:auto;
        }
        th {
            padding: 10px 20px;
            background: #56433D;
            color: #F9C941;
            border-right: 2px solid;
            font-size: 0.9em;
        }
        th:first-child {
            text-align: left;
        }
        th:last-child {
            border-right: none;
        }
        td {
            vertical-align: middle;
            padding: 10px;
            font-size: 14px;
            text-align: center;
            border-top: 2px solid #56433D;
            border-bottom: 2px solid #56433D;
            border-right: 2px solid #56433D;
        }
        td:first-child {
            border-left: 2px solid #56433D;
            border-right: none;
        }
        td:nth-child(2){
            text-align: left;
        }
        form{
            margin-top:15px;
        }
        #exit{
            position: absolute;
            margin-left: 450px;
            color:white;
        }
        #exit:hover{
            background: white;
            color:black;
        }
        #top{
            background: url(/img/bgr.jpg) no-repeat center center;
            background-attachment:fixed;
            background-size:cover;
            height: 300px;
        }
        #top h3{
            background: #000000;
        }
        ul {
            list-style-type: none;
            margin: 0;
            margin-top:-10px;
            padding: 0;
            width: 15%;
            background-color: white;
            position: fixed;
            height: 35%;
            overflow: auto;
        }

        li a {
            display: block;
            color: #000;
            padding: 8px 0 8px 16px;
            text-decoration: none;
        }

        li a.active {
            background-color: #56433D;
            color: #F9C941;
        }

        li a:hover:not(.active) {
            background-color: #555;
            color: white;
        }
    </style>
    <title></title>
</head>
<body>
<div id = "top">
    <h3 style="color:white;">${user} <fmt:message key="message.identified" bundle="${loc}"/><a id = "exit" href="controller?command=logout"><fmt:message key="message.exitLink" bundle="${loc}"/></a>
    </h3>
    <br/>
</div>
<hr/>
<ul>
    <li><a href="controller?command=banlink"><fmt:message key="message.banlink" bundle="${loc}"/></a></li>
    <li><a class="active" href="controller?command=addlink"><fmt:message key="message.addlink" bundle="${loc}"/></a></li>
    <li><a href="controller?command=confirmlink"><fmt:message key="message.confirmlink" bundle="${loc}"/></a></li>
</ul>
<br/>
<br/>
<fmt:message key="message.availableDishesListIntro" bundle="${loc}"/><br/>
<table>
    <tr>
        <th><fmt:message key="message.dishName" bundle="${loc}"/></th>
        <th><fmt:message key="message.price" bundle="${loc}"/></th>
        <th><fmt:message key="message.isAvailable" bundle="${loc}"/> </th>
    </tr>
    <c:forEach var ="map" items="${list}" >
        <tr>
            <td><label for = "${map.key}">${map.key}</label></td>
            <td><label for = "${map.key}">${map.value[0]}</label></td>
            <td><label for = "${map.key}">${map.value[1]}</label></td>
            <td><form name = "deleteButton" method="POST" action="controller" >
                <input type="hidden" name="command" value="change"/>
                <input type="hidden" name="dishToDel" value ="${map.key}"/>
                <input type="submit" value="${map.value[2]}"><br>
            </form></td>
        </tr>
    </c:forEach>
</table>
<br/>
<form name = "addDeleteButtons" method="POST" action="controller" >
    <input type="hidden" name="command" value="change" />
    <fmt:message key="message.changeMenuMessage" bundle="${loc}"/><br/>
    <fmt:message key="message.enterDish" bundle="${loc}"/><input type="text"
                                                                 name="dishToAdd"
                                                                 value=""
                                                                 pattern="(?=.*[a-z]).{4,}"
                                                                 title="<fmt:message key="message.loginTitle" bundle="${loc}"/>"
                                                                 required  /><br/>
    <fmt:message key="message.enterPrice" bundle="${loc}"/><input type="text"
                                                                  name="price"
                                                                  pattern="\d*"
                                                                  title="<fmt:message key="message.priceTitle" bundle="${loc}"/>"
                                                                  required /><br/>
    <input type="submit" value="<fmt:message key="message.addDish" bundle="${loc}"/> "/> <br/>
    ${addDishSuccess}${addDishFail}${editMenuWrongInput}
</form>
<br/>
</body>
<fctg:copyrightInfoTag/>
</html>
