<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.message" var="loc"/>
<fmt:requestEncoding value="UTF-8" />
<html>
<head>
    <title><fmt:message key="message.regPage" bundle="${loc}"/></title>
    <link href="style/menustyle.css" type="text/css" rel="stylesheet">
    <style>
        .letter {
            color: red;
        }
    </style>
</head>
<body>
<section class = "container">
<div class = "login">
<form name="regForm" method="POST" action="controller">
    <input type="hidden" name="command" value="register" />
    <label>
        <fmt:message key="message.login" bundle="${loc}"/><span class="letter">*</span>:<br/>
        <input type="text"
               name="login"
               value=""
               pattern="(?=.*[a-z]).{4,}" required
               title="<fmt:message key="message.loginTitle" bundle="${loc}"/>"
                />
    </label>
    <label>
        <br/><fmt:message key="message.password" bundle="${loc}"/><span class="letter">*</span>:<br/>
        <input type="password"
               pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}" required
               name="password"
               value=""
               title="<fmt:message key="message.passWarning" bundle="${loc}"/>"
                />
    </label>
    <label>
        <br/><fmt:message key="message.repeatPassword" bundle="${loc}"/><span class="letter">*</span>:<br/>
        <input type="password"
               pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}" required
               name="repeatpassword"
               value=""
               title="<fmt:message key="message.passWarning" bundle="${loc}"/>"
                />
    </label>
    <br/> <span class="letter"><fmt:message key="message.reqFields" bundle="${loc}"/></span>
    <br/> ${successRegistrationMessage} ${emptyLoginMessage}
    <br/> ${errorRegistrationMessage}${passwordsMismatch}
    <br/> ${wrongAction}
    <br/> ${nullPage}
    <br/>
    <input type="submit" value="<fmt:message key="message.registerButton" bundle="${loc}"/>"/>
    <br/>

    <br/><a href="controller?command=back"><fmt:message key="message.backToLogin" bundle="${loc}"/></a>
</form>
</div>
<hr/>

</body>
</html>
